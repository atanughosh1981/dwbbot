// Description:
// Digital Workbench Command Interface 
// Helps Rapid Application Development by accelarating each project phases 
// 
// Commands:
// DWB <operation> [-<parameter key> <parameter value> ...] - runs workbench commands based on the operation specified
// DWB -operation BootstrapBase -projectBase againdwb
//
// DWB -operation BootstrapGateway -projectBase againdwb -gatewayName drybank -jdlLocation /Users/atanughosh/Documents/starter/bank-jdl-jhipster.jh
//
// DWB -operation BootstrapService -projectBase againdwb -serviceName dryaccount -jdlLocation /Users/atanughosh/Documents/starter/account-jdl-jhipster.jh
//
// DWB -operation BootstrapService -projectBase againdwb -serviceName drytransaction -jdlLocation /Users/atanughosh/Documents/starter/transaction-jdl-jhipster.jh

var PROJECT_BASE_FOLDER = '/home/workbenchbot/workspace/';
var STARTER_SCRIPT = '/home/workbenchbot/digital-workbench-kickstarter/kickstart.py';

module.exports = function(robot) {
  robot.respond("/RUN (.*)$/i", function(msg) {
    console.log(msg);
    var cmd = msg.match[1];
    var comd = '-o ImportJdl -b /Users/atanughosh/Documents/demoauto/testbot/ -g bank1 -s account -j /Users/atanughosh/Documents/starter/bank-jdl-jhipster.jh'
    msg.send("Running " + comd);
    var spawn = require('child_process').spawn,
        ls    = spawn('/Users/atanughosh/Documents/starter/kickstart.py', ['-o', 'ImportJdl', '-b', '/Users/atanughosh/Documents/demoauto/testbot/', '-g', 'bank1', '-s', 'account','-j', '/Users/atanughosh/Documents/starter/bank-jdl-jhipster.jh']);

    ls.stdout.on('data', function (data) {
      msg.send('stdout: ' + data.toString());
    });

    ls.stderr.on('data', function (data) {
      msg.send('stderr: ' + data.toString());
    });

    ls.on('exit', function (code) {
      msg.send('child process exited with code ' + code.toString());
    });

  });



  robot.respond("/project operation (.*):(.*):(.*):(.*):(.*):(.*):(.*)$/i", function(msg) {
    console.log(msg);
    var operation = msg.match[1]
    var project   = msg.match[2]
    var gateway   = msg.match[3]
    var service   = msg.match[4]
    var jdlFile   = msg.match[5]
    var gitlabuser   = msg.match[6]
    var gitlabproj   = msg.match[7]

    msg.send ("Start Bootstrap... Setting up required files and libraries... " + gateway)

    var spawn = require('child_process').spawn,
        cmd   = spawn('/Users/atanughosh/Documents/starter/kickstart.py', ['-o', operation, '-b', '/Users/atanughosh/Documents/demoauto/' + project +'/', 
									   '-g', gateway, '-s', service, '-j', jdlFile, '-u', gitlabuser, '-p', gitlabproj]);
    cmd.stdout.on('data', function (data) {
      msg.send('stdout: ' + data.toString());
    });

    cmd.stderr.on('data', function (data) {
      msg.send('stderr: ' + data.toString());
    });

    cmd.on('exit', function (code) {
      msg.send('child process exited with code ' + code.toString());
    });

  });

  //robot.respond("/Workbench (.*) Data:(.*)$/i", function(msg) {
  robot.respond("/Workbench (.*) Data:((.|\s|\n)+)/i", function(msg) {
    console.log(msg);
    var operation = msg.match[1]
    var jsondata = msg.match[2]

    // msg.send ("Operation: " + operation + ", Data Input: " + jsondata);
    
    parsedjson = JSON.parse(jsondata)

    var spawn = {};
    var dummy = 'dummy'
    projectBase = '/Users/atanughosh/Documents/demoauto/' + parsedjson.projectBase
    switch(operation){
        case "BootstrapBase":
            spawn = require('child_process').spawn,
            cmd   = spawn('/Users/atanughosh/Documents/starter/kickstart.py', ['-o', operation, '-b', projectBase +'/',
                                                                           '-g', dummy, '-s', dummy, '-j', dummy]);
        break;
        case "BootstrapGateway":
            gatewayName = parsedjson.gatewayName
            gatewayJdl = parsedjson.jdlLocation
            spawn = require('child_process').spawn,
            cmd   = spawn('/Users/atanughosh/Documents/starter/kickstart.py', ['-o', operation, '-b', projectBase +'/',
                                                                           '-g', gatewayName, '-s', dummy, '-j', gatewayJdl]);
        break;
        case "BootstrapService":
            serviceName = parsedjson.serviceName
            serviceJdl = parsedjson.jdlLocation
            spawn = require('child_process').spawn,
            cmd   = spawn('/Users/atanughosh/Documents/starter/kickstart.py', ['-o', operation, '-b', projectBase +'/',
                                                                           '-g', dummy, '-s', serviceName, '-j', serviceJdl]);
        break;
        default: 
            console.log('default');
        break;
    }

    cmd.stdout.on('data', function (data) {
      msg.send(data.toString());
    });

    cmd.stderr.on('data', function (data) {
      msg.send('ERROR: ' + data.toString());
    });

    cmd.on('exit', function (code) {
      msg.send('Operartion, ' + operation  + ' Completed. Success Code: ' + code.toString());
    });
  });

robot.respond("/DWB ((.|\s|\n)+)/i", function(msg) {
    console.log(msg);
    var userInput = msg.match[1]
    let inputArray = userInput.split(/\s+/)

    // Parameterized Input parsing
    parsedjson = {}
    for (let index = 0; index < inputArray.length; index++) {
        let key = (inputArray[index].split('-')[1]);
        let val = inputArray[++index];
        //msg.send ("Key: " + key + ", Value: " + val);
        parsedjson [key] = val;
    }

    var spawn = {};
    var dummy = 'dummy'
    projectBase = PROJECT_BASE_FOLDER + parsedjson.projectBase

    operation = parsedjson.operation
    if (operation === null || operation === '') {
        msg.send("`No/Invalid Operation parameter specified. Please use WORKBENCH -h for more information`");
    }
    
    switch(operation){
        case "BootstrapBase":
            msg.send  ("```Operation: " + parsedjson.operation + ", Base Folder: " + projectBase + "```");
            spawn = require('child_process').spawn,
            cmd   = spawn(STARTER_SCRIPT, ['-o', operation, '-b', projectBase +'/',
                                                                           '-g', dummy, '-s', dummy, '-j', dummy]);
        break;
        case "BootstrapInfra":
            gitUsername = parsedjson.cicdUsername
            gitPassword = parsedjson.cicdPassword

            dockerUsername = parsedjson.dockerUsername
            dockerPassword = parsedjson.dockerPassword

            gcloudProject = parsedjson.cloudProject
            gcloudClusterName = parsedjson.cloudClusterName
            gcloudClusterZone = parsedjson.cloudClusterZone

            msg.send  ("```Operation: " + parsedjson.operation + ", Base Folder: " + projectBase + "```");
            spawn = require('child_process').spawn,
            cmd   = spawn(STARTER_SCRIPT, ['-o', operation, '-b', projectBase +'/',
                                           '-e', gitUsername, '-f', gitPassword, '-m', dockerUsername, 
                                           '-n', dockerPassword, '-p', gcloudProject, '-q', gcloudClusterName, '-r', gcloudClusterZone]);
        break;
        case "BootstrapGateway":
            gatewayName = parsedjson.gatewayName
            gatewayJdl = parsedjson.jdlLocation
            msg.send  ("```Operation: " + operation + ", Base Folder: " + projectBase +  " Gateway Application: " +  gatewayName + ", Entity File: " + gatewayJdl + "```");
            spawn = require('child_process').spawn,
            cmd   = spawn(STARTER_SCRIPT, ['-o', operation, '-b', projectBase +'/',
                                                                           '-g', gatewayName, '-s', dummy, '-j', gatewayJdl]);
        break;
        case "BootstrapService":
            serviceName = parsedjson.serviceName
            serviceJdl = parsedjson.jdlLocation
            msg.send  ("```Operation: " + operation + ", Base Folder: " + projectBase +  " Microservice Application: " +  serviceName + ", Entity File: " + serviceJdl + "```");
            spawn = require('child_process').spawn,
            cmd   = spawn(STARTER_SCRIPT, ['-o', operation, '-b', projectBase +'/',
                                                                           '-g', dummy, '-s', serviceName, '-j', serviceJdl]);
        break;
        default:
            msg.send("`Operation: " + operation + " not Supported`");
        break;
    }

    let formatStarted  = false;
    cmd.stdout.on('data', function (data) {
        msg.send("```" + data.toString() + "```");
    });

    cmd.stderr.on('data', function (data) {
      msg.send('`ERROR: ' + data.toString() + '`');
    });

    cmd.on('exit', function (code) {
      msg.send('```Operation: ' + operation  + ' completed. Execution Code: ' + code.toString() + "```");
    });

  });
}

