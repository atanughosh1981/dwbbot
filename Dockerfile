FROM jhipster/jhipster

RUN echo $(pwd)
RUN echo $(ls -ltr)
# Set the working directory to /workbench-kickstarter
ENV BOT_PATH "/home/workbenchbot"
ENV PROJECT_BASE "${BOT_PATH}/workspace/"
USER jhipster
WORKDIR $BOT_PATH
RUN echo $(pwd)
RUN echo $(whoami)

RUN mkdir -p ${BOT_PATH} && echo $(ls)

# Copy the current directory contents into the container at /workbench-kickstarter
ADD . ${BOT_PATH}

# Environment variables
ENV DEBIAN_FRONTEND noninteractive 
ENV HUBOT_SLACK_TOKEN xoxb-437578199906-437578799362-jIsg3oSCgQhs1NBQPxJpM6hz
ENV HUBOT_NAME myhubot
ENV HUBOT_OWNER none
ENV HUBOT_DESCRIPTION Hubot

USER root
# Create code genertor base folder
RUN mkdir -p ${PROJECT_BASE}

# Installing the Bot runner now
RUN npm install -g yo hubot generator-hubot
RUN chown -R jhipster:jhipster \
    ${BOT_PATH} \
    /usr/lib/node_modules

RUN mkdir -p ${BOT_PATH}/myhubot && cd ${BOT_PATH}/myhubot  && echo $(ls) && echo $(whoami) \
yo hubot --owner="${HUBOT_OWNER}" --name="${HUBOT_NAME}" --description="${HUBOT_DESCRIPTION}" --adapter slack --defaults

VOLUME ["${BOT_PATH}/myhubot/scripts/",]
RUN cp ./digital-workbench.js ./scripts/

CMD bin/hubot -n $HUBOT_NAME --adapter slack
